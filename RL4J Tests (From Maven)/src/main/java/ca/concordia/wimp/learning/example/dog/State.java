/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 * Copyright (c) 2019 Ptidej Team
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package ca.concordia.wimp.learning.example.dog;

import org.deeplearning4j.rl4j.space.Encodable;

/**
 * @author rubenfiszel (ruben.fiszel@epfl.ch) 7/18/16.
 * @author Yann-Gaël Guéhéneuc, 2019/09/20
 */

public class State implements Encodable {
	private static final double[] ConvertMatrixToArray(
			final double[][] matrix) {

		final double array[] = new double[matrix.length * matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			final double[] row = matrix[i];
			for (int j = 0; j < row.length; j++) {
				array[i * row.length + j] = matrix[i][j];
			}
		}

		return array;
	}

	private final double[][] room = new double[IProblemConstants.ROOM_WIDTH][IProblemConstants.ROOM_HEIGHT];
	private int currentDogCol;
	private int currentDogRow;

	public State(final int dogCol, final int dogRow) {
		// Remove the dog
		this.room[this.currentDogCol][this.currentDogRow] = 0;

		// Put the dog in its new location
		this.room[dogCol][dogRow] = 1;
		this.currentDogCol = dogCol;
		this.currentDogRow = dogRow;

		/*
		 * System.out.print("("); System.out.print(this.currentDogCol + 1);
		 * System.out.print(", "); System.out.print(this.currentDogRow + 1);
		 * System.out.println(")");
		 */
	}
	public int getCurrentDogCol() {
		return this.currentDogCol;
	}
	public int getCurrentDogRow() {
		return this.currentDogRow;
	}

	@Override
	public double[] toArray() {
		return State.ConvertMatrixToArray(room);
	}
}