package ca.concordia.wimp.learning.example.toy2;

import java.io.IOException;

import org.deeplearning4j.rl4j.learning.Learning;
import org.deeplearning4j.rl4j.learning.sync.qlearning.QLearning;
import org.deeplearning4j.rl4j.learning.sync.qlearning.discrete.QLearningDiscreteDense;
import org.deeplearning4j.rl4j.network.dqn.DQNFactoryStdDense;
import org.deeplearning4j.rl4j.network.dqn.IDQN;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.util.DataManager;
import org.nd4j.linalg.learning.config.Adam;

public class ASimpleToy2 {
	public static void main(final String[] args) throws IOException {
		final DQNFactoryStdDense.Configuration network = DQNFactoryStdDense.Configuration
				.builder().l2(0.01).updater(new Adam(1e-2)).numLayer(3)
				.numHiddenNodes(16).build();

		final QLearning.QLConfiguration qLearning = new QLearning.QLConfiguration(
				// Random seed
				123, 100000, // Max step by epoch
				80000, // Max numbers of step
				10000, // Max size of experience replay
				32, // size of batches
				100, // target update (hard)
				0, // num step noop warmup
				0.05, // reward scaling
				0.99, // gamma
				10.0, // td-error clipping
				0.1f, // min epsilon
				2000, // num step for eps greedy anneal
				true // double DQN
		);

		// record the training data in rl4j-data in a new folder
		DataManager manager = new DataManager();

		// define the mdp from toy (toy length)
		ASimpleToy2MDP mdp = new ASimpleToy2MDP(20);

		// define the training method
		Learning<ASimpleToy2State, Integer, DiscreteSpace, IDQN> dql = new QLearningDiscreteDense<ASimpleToy2State>(
				mdp, network, qLearning, manager);

		// enable some logging for debug purposes on toy mdp
		mdp.setFetchable(dql);

		// start the training
		dql.train();

		// useless on toy but good practice!
		mdp.close();
	}
}
