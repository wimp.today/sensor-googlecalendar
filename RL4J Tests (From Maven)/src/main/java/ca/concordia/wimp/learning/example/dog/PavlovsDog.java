package ca.concordia.wimp.learning.example.dog;

import java.io.IOException;

import org.deeplearning4j.rl4j.learning.sync.qlearning.QLearning;
import org.deeplearning4j.rl4j.learning.sync.qlearning.discrete.QLearningDiscreteDense;
import org.deeplearning4j.rl4j.network.dqn.DQNFactoryStdDense;
import org.deeplearning4j.rl4j.util.DataManager;
import org.nd4j.linalg.learning.config.Adam;

public class PavlovsDog {
	private static final DQNFactoryStdDense.Configuration NETWORK = DQNFactoryStdDense.Configuration
			.builder().l2(0.01).updater(new Adam(1e-2)).numLayer(3)
			.numHiddenNodes(16).build();
	private static final QLearning.QLConfiguration Q_LEARNING = new QLearning.QLConfiguration(
			123, // Random seed
			100000, // Max number of steps by epoch
			5000, // Max number of steps
			10000, // Max size of experience replay
			32, // size of batches
			100, // target update (hard)
			0, // num step noop warmup
			0.05, // reward scaling
			0.99, // gamma
			10.0, // td-error clipping
			0.1f, // min epsilon
			2000, // num step for eps greedy anneal
			true // double DQN
	);
	private static final DataManager DATA_MANAGER;
	static {
		DataManager temp;
		try {
			temp = new DataManager();
		} catch (final IOException e) {
			e.printStackTrace();
			temp = null;
		}
		DATA_MANAGER = temp;
	}

	public static void main(final String[] args) throws IOException {
		final MarkovDecisionProcess mdp = new MarkovDecisionProcess();

		final QLearningDiscreteDense<State> qldd = new QLearningDiscreteDense<State>(
				mdp, NETWORK, Q_LEARNING, DATA_MANAGER);
		mdp.setFetchable(qldd);
		qldd.train();
		mdp.close();
	}
}
