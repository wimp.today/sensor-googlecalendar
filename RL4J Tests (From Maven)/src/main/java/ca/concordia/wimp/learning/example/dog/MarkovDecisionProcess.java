/*******************************************************************************
 * Copyright (c) 2015-2018 Skymind, Inc.
 * Copyright (c) 2019 Ptidej Team
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package ca.concordia.wimp.learning.example.dog;

import org.deeplearning4j.gym.StepReply;
import org.deeplearning4j.rl4j.learning.NeuralNetFetchable;
import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.network.dqn.IDQN;
import org.deeplearning4j.rl4j.space.ArrayObservationSpace;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.space.ObservationSpace;
import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rubenfiszel (ruben.fiszel@epfl.ch) 7/18/16.
 * @author Yann-Gaël Guéhéneuc, 2019/09/20
 */

public class MarkovDecisionProcess
		implements
			MDP<State, Integer, DiscreteSpace> {

	private State state;
	private boolean isDone;

	@Getter
	private DiscreteSpace actionSpace = new DiscreteSpace(8);

	@Getter
	private ObservationSpace<State> observationSpace = new ArrayObservationSpace<State>(
			new int[]{IProblemConstants.ROOM_WIDTH * IProblemConstants.ROOM_HEIGHT});

	@SuppressWarnings("rawtypes")
	@Setter
	private NeuralNetFetchable<IDQN> fetchable;

	@Override
	public void close() {
	}

	@Override
	public boolean isDone() {
		return this.isDone;
	}

	@Override
	public MarkovDecisionProcess newInstance() {
		final MarkovDecisionProcess mdp = new MarkovDecisionProcess();
		mdp.setFetchable(fetchable);
		return mdp;
	}

	@Override
	public State reset() {
		this.isDone = false;
		return state = new StateOrigin();
	}

	@Override
	public StepReply<State> step(final Integer actionIndex) {
		int reward;
		int newDogCol;
		int newDogRow;

		switch (actionIndex) {
			case 0 :
				// Going north-west
				newDogCol = this.state.getCurrentDogCol() - 1;
				newDogRow = this.state.getCurrentDogRow() - 1;
				break;

			case 1 :
				// Going north
				newDogCol = this.state.getCurrentDogCol();
				newDogRow = this.state.getCurrentDogRow() - 1;
				break;

			case 2 :
				// Going north-east
				newDogCol = this.state.getCurrentDogCol() + 1;
				newDogRow = this.state.getCurrentDogRow() - 1;
				break;

			case 3 :
				// Going west
				newDogCol = this.state.getCurrentDogCol() - 1;
				newDogRow = this.state.getCurrentDogRow();
				break;

			case 4 :
				// Going east
				newDogCol = this.state.getCurrentDogCol() + 1;
				newDogRow = this.state.getCurrentDogRow();
				break;

			case 5 :
				// Going south-west
				newDogCol = this.state.getCurrentDogCol() - 1;
				newDogRow = this.state.getCurrentDogRow() + 1;
				break;

			case 6 :
				// Going south
				newDogCol = this.state.getCurrentDogCol();
				newDogRow = this.state.getCurrentDogRow() + 1;
				break;

			case 7 :
				// Going south-east
				newDogCol = this.state.getCurrentDogCol() + 1;
				newDogRow = this.state.getCurrentDogRow() + 1;
				break;

			default :
				throw new RuntimeException("This dog belongs to Schrödinger!");
		}

		if (newDogCol < 0 || newDogCol >= IProblemConstants.ROOM_WIDTH || newDogRow < 0
				|| newDogRow >= IProblemConstants.ROOM_HEIGHT) {

			// If the dog bangs into a wall, then it cannot move...
			// TODO Check also for the interior walls!
			newDogCol = this.state.getCurrentDogCol();
			newDogRow = this.state.getCurrentDogRow();
			// ... and it is penalized!
			reward = -2;
		} else if (newDogCol == 4 && newDogRow == 4) {
			// If the dog found the chicken drumstick,
			// the it is rewarded and the learning is done.
			reward = 100;
			this.isDone = true;
		} else {
			reward = -1;
		}

		this.state = new State(newDogCol, newDogRow);
		return new StepReply<>(this.state, reward, this.isDone,
				new JSONObject("{}"));
	}
}
