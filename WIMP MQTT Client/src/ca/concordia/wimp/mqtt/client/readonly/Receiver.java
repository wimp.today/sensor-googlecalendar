package ca.concordia.wimp.mqtt.client.readonly;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Receiver {
	public static void main(final String[] args) {
		try {
			final IMqttClient client = new MqttClient("tcp://localhost:1883",
					MqttClient.generateClientId());
			client.setCallback(new MqttCallback() {
				@Override
				public void messageArrived(final String s, final MqttMessage m)
						throws Exception {

					System.out.println("Message received:\n\t"
							+ new String(m.getPayload()));
				}

				@Override
				public void deliveryComplete(final IMqttDeliveryToken t) {
				}

				@Override
				public void connectionLost(final Throwable t) {
					System.out.println("Connection to MQTT broker lost!");
				}
			});
			client.connect();
			client.subscribe("sensors/temperature");
			// client.disconnect();
			// client.close();
		} catch (final MqttException e) {
			e.printStackTrace();
		}
	}
}
