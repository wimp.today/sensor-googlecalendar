package ca.concordia.wimp.sensor.googlecalendar;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.*;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;

import ca.concordia.wimp.sensor.ISensor;

public class Sensor implements ISensor {
	private static final String APPLICATION_NAME = "WIMP Sensor Google Calendar";
	private static final JsonFactory JSON_FACTORY = JacksonFactory
			.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";
	

	/**
	 * Global instance of the scopes required by this quickstart. If modifying
	 * these scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections
			.singletonList(CalendarScopes.CALENDAR);
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT
	 *            The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException
	 *             If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(
			final NetHttpTransport HTTP_TRANSPORT) throws IOException {

		// Load client secrets.
		final InputStream in = Sensor.class
				.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException(
					"Resource not found: " + CREDENTIALS_FILE_PATH);
		}

		final GoogleClientSecrets clientSecrets = GoogleClientSecrets
				.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
				.setDataStoreFactory(new FileDataStoreFactory(
						new java.io.File(TOKENS_DIRECTORY_PATH)))
				.setAccessType("offline").build();
		final LocalServerReceiver receiver = new LocalServerReceiver.Builder()
				.setPort(8888).build();

		return new AuthorizationCodeInstalledApp(flow, receiver)
				.authorize("user");
	}

	private List<Event> getEvents(int number_events)
			throws GeneralSecurityException, IOException {
		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport
				.newTrustedTransport();

		final Calendar service = new Calendar.Builder(HTTP_TRANSPORT,
				JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();

		// List the next 10 events from the primary calendar.
		final DateTime now = new DateTime(System.currentTimeMillis());
		final Events events = service.events().list("primary").setMaxResults(number_events)
				.setTimeMax(now).setOrderBy("startTime").setSingleEvents(true)
				.execute();
		return events.getItems();
	}

	
	
	private static void PrintListCalanders()
			throws GeneralSecurityException, IOException{


		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport
				.newTrustedTransport();

		final Calendar service = new Calendar.Builder(HTTP_TRANSPORT,
				JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();


		// Iterate through entries in calendar list
		String pageToken = null;
		do {
			CalendarList calendarList = service.calendarList().list().setPageToken(pageToken).execute();
			List<CalendarListEntry> items = calendarList.getItems();

			for (CalendarListEntry calendarListEntry : items) {
				System.out.println(calendarListEntry.getSummary());
			}
			pageToken = calendarList.getNextPageToken();
		} while (pageToken != null);

	}
	
	
	private static void AddCalenderID(String CalenderID) throws GeneralSecurityException, IOException {
		
		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport
			.newTrustedTransport();

		

		// Create a new calendar
		Calendar s = new Calendar.Builder(HTTP_TRANSPORT,
				JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(APPLICATION_NAME).build();
		
	
		
		// Create a new calendar
		
		/*
		Calendar calendar = new Calendar();
		calendar.setSummary("calendarSummary");
		calendar.setTimeZone("America/Los_Angeles");

		// Insert the new calendar
		Calendar createdCalendar = service.calendars().insert(calendar).execute();

		System.out.println(createdCalendar.getId());
		*/
	}



	private static void AddEvent(String calendarId, String summary, String location, String description, String ThestartDateTime, String TheEndDateTime ) throws GeneralSecurityException, IOException {
		
		
		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport
						.newTrustedTransport();

		final Calendar service = new Calendar.Builder(HTTP_TRANSPORT,
						JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
						.setApplicationName(APPLICATION_NAME).build();
		
		Event event = new Event()
			    .setSummary(summary)
			    .setLocation(location)
			    .setDescription(description);

		//DateTime startDateTime = new DateTime("2019-05-28T09:00:00-07:00");
		DateTime startDateTime = new DateTime(ThestartDateTime);
		EventDateTime start = new EventDateTime()
		    .setDateTime(startDateTime)
		    .setTimeZone("America/Los_Angeles");
		event.setStart(start);

		//DateTime endDateTime = new DateTime("2019-05-28T17:00:00-07:00");
		DateTime endDateTime = new DateTime(ThestartDateTime);
		EventDateTime end = new EventDateTime()
		    .setDateTime(endDateTime)
		    .setTimeZone("America/Los_Angeles");
		event.setEnd(end);

			//String[] recurrence = new String[] {"RRULE:FREQ=DAILY;COUNT=2"};
			//event.setRecurrence(Arrays.asList(recurrence));

			//EventAttendee[] attendees = new EventAttendee[] {
			//    new EventAttendee().setEmail("lpage@example.com"),
			//    new EventAttendee().setEmail("sbrin@example.com"),
			// };
			//event.setAttendees(Arrays.asList(attendees));

			EventReminder[] reminderOverrides = new EventReminder[] {
			    new EventReminder().setMethod("email").setMinutes(24 * 60),
			    new EventReminder().setMethod("popup").setMinutes(10),
			};
			Event.Reminders reminders = new Event.Reminders()
			    .setUseDefault(false)
			    .setOverrides(Arrays.asList(reminderOverrides));
			event.setReminders(reminders);

			//String calendarId = "ptidej.team.iot@gmail.com";
			event = service.events().insert(calendarId, event).execute();
			System.out.printf("Event created: %s\n", event.getHtmlLink());
		
		
	}
	
	
private static void DeleteEvent(String calendarId, String summary) throws GeneralSecurityException, IOException {
		
		
		// Build a new authorized API client service.
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport
						.newTrustedTransport();

		final Calendar service = new Calendar.Builder(HTTP_TRANSPORT,
						JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
						.setApplicationName(APPLICATION_NAME).build();
		
		// Delete an event
		service.events().delete(calendarId, summary).execute();
	}


	private static String GetEventIDFromName(String EventName, String CalanderId) throws GeneralSecurityException, IOException {
		
		// Build a new authorized API client service.
				final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport
								.newTrustedTransport();

				final Calendar service = new Calendar.Builder(HTTP_TRANSPORT,
								JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
								.setApplicationName(APPLICATION_NAME).build();
				
				
				final Events events = service.events().list(CalanderId).execute();
				
				final List<Event> items = events.getItems();
				if (items.isEmpty()) {
					System.out.println("No events found.");
				} else {
					
					for (final Event event : items) {
						DateTime start = event.getStart().getDateTime();
						
						System.out.println("Event Description= " +event.getSummary()+" ID=" + event.getId());
						if (event.getSummary().equals(EventName) ) {
							
							System.out.println("Event Id= " + event.getId());
							return event.getId();
						}
						
					}
				}
				
		
		return null;
	}


	public static void main(final String... args)
			throws IOException, GeneralSecurityException {

		final Sensor sensor = new Sensor();
		
		
		PrintListCalanders();
		
		AddEvent("ptidej.team.iot@gmail.com", "MyNewEvent3", "concordia",
				"This is a test event", "2019-06-20T09:00:00-07:00", "2019-06-20T17:00:00-07:00");
		
		System.out.println("Event id= "+ GetEventIDFromName("event 3", "ptidej.team.iot@gmail.com"));
		
		
		//DeleteEvent("ptidej.team.iot@gmail.com", "de2069co89ls01hicrm0geaqn8");
		
		
		
		//AddCalenderID("test_calander");
 
		final List<Event> items = sensor.getEvents(10);
		if (items.isEmpty()) {
			System.out.println("No upcoming events found.");
		} else {
			System.out.println("Upcoming events");
			for (final Event event : items) {
				DateTime start = event.getStart().getDateTime();
				if (start == null) {
					start = event.getStart().getDate();
				}
				System.out.printf("%s (%s) %s\n", event.getSummary(), start, event.getId());
			}
		}
	}
}