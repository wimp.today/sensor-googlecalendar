package ca.concordia.wimp.mqtt.broker;

import org.eclipse.paho.client.mqttv3.MqttException;

import ca.concordia.wimp.mqtt.client.emitter.Emitter;
import ca.concordia.wimp.mqtt.client.readonly.Receiver;
import junit.framework.TestCase;

public class StressTest1 extends TestCase {
	public void test1() throws MqttException {
		// Start the broker
		MosquitoBrokerLauncher.main(null);

		// Start one listener
		Receiver.main(null);

		// Start one emitter
		final long currentTime = System.currentTimeMillis();
		for (int i = 0; i < 100; i++) {
			Emitter.main(null);
		}
		System.out.print("100 messages sent in ");
		System.out.print(System.currentTimeMillis() - currentTime);
		System.out.println(" ms.");
	}
	public void test2() throws MqttException {
		// Start the broker
		MosquitoBrokerLauncher.main(null);

		// Start one listener
		Receiver.main(null);

		// Start one emitter
		final Emitter emitter = new Emitter();
		emitter.connect();
		final long currentTime = System.currentTimeMillis();
		for (int i = 0; i < 100; i++) {
			emitter.publish("temperature/sensor",
					String.valueOf(Math.random()));
		}
		emitter.disconnect();
		System.out.print("100 messages sent in ");
		System.out.print(System.currentTimeMillis() - currentTime);
		System.out.println(" ms.");
	}
}
