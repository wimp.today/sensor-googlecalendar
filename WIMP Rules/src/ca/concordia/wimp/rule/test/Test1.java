package ca.concordia.wimp.rule.test;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.RuleBuilder;

import junit.framework.TestCase;

public class Test1 extends TestCase {
	public void test1() {
		// From https://github.com/j-easy/easy-rules

		// define facts
		Facts facts = new Facts();
		facts.put("rain", true);

		// define rules
		Rule weatherRule = new RuleBuilder().name("weather rule")
				.description("if it rains then take an umbrella")
				.when(facts2 -> facts.get("rain").equals(true))
				.then(facts2 -> System.out
						.println("It rains, take an umbrella!"))
				.build();
		Rules rules = new Rules();
		rules.register(weatherRule);

		// fire rules on known facts
		RulesEngine rulesEngine = new DefaultRulesEngine();
		rulesEngine.fire(rules, facts);
	}
	public void test2() {
		// From https://github.com/j-easy/easy-rules

		// define facts
		Facts facts = new Facts();
		facts.put("rain", true);

		// define rules
		Rule weatherRule = new RuleBuilder().name("weather rule")
				.description("if it rains then take an umbrella")
				.when(facts2 -> facts.get("rain").equals(true))
				.then(facts2 -> System.out
						.println("It rains, take an umbrella!"))
				.when(facts2 -> facts.get("rain").equals(false))
				.then(facts2 -> System.out
						.println("Sunshine!"))
				.build();
		Rules rules = new Rules();
		rules.register(weatherRule);

		// fire rules on known facts
		RulesEngine rulesEngine = new DefaultRulesEngine();
		rulesEngine.fire(rules, facts);

		facts.put("rain", false);
		rulesEngine.fire(rules, facts);
	}
}
