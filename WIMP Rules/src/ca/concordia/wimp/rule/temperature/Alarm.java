package ca.concordia.wimp.rule.temperature;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.jeasy.rules.api.Condition;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.RuleBuilder;

public class Alarm {
	public static void main(final String[] args) throws MqttException {
		final Rule heatAlarm = new RuleBuilder().name("Heat Alarm")
				.description("If temperature above 40, raise an alarm!")
				.when(new Condition() {
					@Override
					public boolean evaluate(final Facts facts) {
						int t = facts.get("t");
						return t >= 40;
					}
				}).then(action -> System.out.println("Heat-wave alarm!"))
				.build();

		final Rule heatNormal = new RuleBuilder().name("Normal Heat")
				.description("If temperature below 40, cooool!")
				.when(new Condition() {
					@Override
					public boolean evaluate(final Facts facts) {
						int t = facts.get("t");
						return t < 40;
					}
				}).then(action -> System.out.println("Normal temperature"))
				.build();
		final Rules rules = new Rules();
		rules.register(heatAlarm);
		rules.register(heatNormal);
		final RulesEngine rulesEngine = new DefaultRulesEngine();
		final Facts facts = new Facts();

		final IMqttClient client = new MqttClient("tcp://localhost:1883",
				MqttClient.generateClientId());
		client.setCallback(new MqttCallback() {
			@Override
			public void messageArrived(final String topic,
					final MqttMessage message) throws Exception {
				if (topic.equals("sensors/temperature")) {
					facts.put("t",
							Integer.valueOf(new String(message.getPayload())));
					rulesEngine.fire(rules, facts);
				}
			}
			@Override
			public void deliveryComplete(final IMqttDeliveryToken t) {
			}
			@Override
			public void connectionLost(final Throwable t) {
				System.out.println("Connection to MQTT broker lost!");
			}
		});

		client.connect();
		client.subscribe("sensors/temperature");
		// client.disconnect();
		// client.close();
	}
}
