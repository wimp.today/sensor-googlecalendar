package ca.concordia.wimp.mqtt.broker;

import util.io.ProxyConsole;
import util.process.CallerHelper;
import util.system.OSValidator;

public class MosquitoBrokerLauncher {
	public static void main(final String[] args) {
		MosquitoBrokerLauncher.start();
	}
	public static void start() {
		if (OSValidator.isWindows()) {
			CallerHelper.execute("Mosquito Broker",
					"\"../WIMP MQTT Broker/exe/Win/mosquitto.exe\"");
		} else {
			ProxyConsole.getInstance().errorOutput().println("Unknown OS");
		}
	}
	public static void stop() {
		// TODO: Impossible?
	}
}
